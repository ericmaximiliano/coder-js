//localStorage.clear()

// inicializaciones
// clase con los atributos de cada persona
class Persona {
    constructor(idPersona, nombre, apellido, edad, dni) {
        this.idPersona = idPersona
        this.nombre = nombre
        this.apellido = apellido
        this.edad = edad
        this.dni = dni
    }
}

// arrays inicializados vacios
let clientes = []
let clientesLS = []
let prestamosOtorgados = []
let prestamosOtorgadosLS = []

// variable auxiliar
let clientesLocalStorage = null
let prestamosLocalStorage = null

// url de API para enviar mails
const urlBaseMail = 'https://api.emailjs.com/api/v1.0/email/send'

// hardcodeo algunos clientes
//creo 5 personas distintas
const persona1 = new Persona(1, "Juan", "Perez", 30, 12345678)
const persona2 = new Persona(2, "Camila", "Fernandez", 22, 22345678)
const persona3 = new Persona(3, "Federico", "Gonzalez", 25, 32345678)
const persona4 = new Persona(4, "Ana", "Rodriguez", 35, 42345678)
const persona5 = new Persona(5, "Federico", "Gallardo", 41, 52345678)

// agrego las 5 personas como clientes
clientes.push({ idCliente: persona1.idPersona, nombreCliente: persona1.nombre, apellidoCliente: persona1.apellido, edadCliente: persona1.edad, dniCliente: persona1.dni })
clientes.push({ idCliente: persona2.idPersona, nombreCliente: persona2.nombre, apellidoCliente: persona2.apellido, edadCliente: persona2.edad, dniCliente: persona2.dni })
clientes.push({ idCliente: persona3.idPersona, nombreCliente: persona3.nombre, apellidoCliente: persona3.apellido, edadCliente: persona3.edad, dniCliente: persona3.dni })
clientes.push({ idCliente: persona4.idPersona, nombreCliente: persona4.nombre, apellidoCliente: persona4.apellido, edadCliente: persona4.edad, dniCliente: persona4.dni })
clientes.push({ idCliente: persona5.idPersona, nombreCliente: persona5.nombre, apellidoCliente: persona5.apellido, edadCliente: persona5.edad, dniCliente: persona5.dni })

// creo array con relacion idPrestamo e idCliente
prestamosOtorgados.push({ idPrestamo: 11, idCliente: persona1.idPersona, monto: 50000, cuotas: 6 })
prestamosOtorgados.push({ idPrestamo: 12, idCliente: persona2.idPersona, monto: 20000, cuotas: 3 })
prestamosOtorgados.push({ idPrestamo: 13, idCliente: persona3.idPersona, monto: 45000, cuotas: 9 })
prestamosOtorgados.push({ idPrestamo: 14, idCliente: persona4.idPersona, monto: 70000, cuotas: 12 })
prestamosOtorgados.push({ idPrestamo: 15, idCliente: persona5.idPersona, monto: 10000, cuotas: 6 })
prestamosOtorgados.push({ idPrestamo: 16, idCliente: persona2.idPersona, monto: 90000, cuotas: 10 })

// muestro menu inicial
document.getElementById("div-solicitarPrestamos").style.display = 'none'
document.getElementById("div-consultarPrestamos").style.display = 'none'
document.getElementById("div-menu-seleccion").style.display = 'block'

// complemento mi base de clientes utilizando fetch sobre un archivo json
obtenerBaseClientes()
// complemento mi base de prestamos utilizando fetch sobre un archivo json
obtenerBasePrestamos()

//eventos
let montoPrestamo = document.getElementById("id-monto")
montoPrestamo.addEventListener("change", validarMonto)

let nombreCliente = document.getElementById("id-nombre")
nombreCliente.addEventListener("change", validarNombre)

let apellidoCliente = document.getElementById("id-apellido")
apellidoCliente.addEventListener("change", validarApellido)

let dniCliente = document.getElementById("id-dni")
dniCliente.addEventListener("change", validarDNI)

let edadCliente = document.getElementById("id-edad")
edadCliente.addEventListener("change", validarEdad)

let mailCliente = document.getElementById("id-email")
mailCliente.addEventListener("change", validarMail)

let btnConfirmarPrestamo = document.getElementById("id-confirmacion")
btnConfirmarPrestamo.addEventListener("click", confirmarPrestamo)

let btnSolicitarPrestamo = document.getElementById("id-btn-conf-prestamo")
btnSolicitarPrestamo.addEventListener("click", mostrarSolicitar)

let btnConsultarPrestamo = document.getElementById("id-btn-cons-prestamo")
btnConsultarPrestamo.addEventListener("click", mostrarConsultar)

const inputBusquedaId = document.getElementById("id-key")
const valorBusquedaId = inputBusquedaId.value
inputBusquedaId.addEventListener("keyup", habilitarBtn)

const btnBusquedaId = document.getElementById("id-busqid")
btnBusquedaId.addEventListener("click", buscarPrestamoPorId)

const inputBusquedaPorNombre = document.getElementById("id-nomape")
inputBusquedaPorNombre.addEventListener("input", buscarPrestamoPorNombre)
inputBusquedaPorNombre.addEventListener("change", limpiarInputNombre)

//FUNCIONES

//funcion principal para hacer todo el calculo de un prestamo, guardar datos y enviar mail
function confirmarPrestamo() {
    // valido que esten cargados todos los datos
    // y en caso que no retorno control al usuario para que los ingrese
    montoPrestamo = document.getElementById("id-monto")
    validarMonto()
    montoPrestamo = montoPrestamo.value
    montoPrestamo = parseInt(montoPrestamo)
    if (montoPrestamo == 0 || isNaN(montoPrestamo)) {
        return
    }

    let cuotasPrestamo = document.getElementById("id-cuotas").value
    cuotasPrestamo = parseInt(cuotasPrestamo)

    nombreCliente = document.getElementById("id-nombre")
    validarNombre()
    nombreCliente = nombreCliente.value
    if (nombreCliente == '') {
        return
    }

    apellidoCliente = document.getElementById("id-apellido")
    validarApellido()
    apellidoCliente = apellidoCliente.value
    if (apellidoCliente == '') {
        return
    }

    dniCliente = document.getElementById("id-dni")
    validarDNI()
    dniCliente = dniCliente.value
    if (dniCliente == '' || isNaN(dniCliente)) {
        return
    }
    dniCliente = parseInt(dniCliente)

    edadCliente = document.getElementById("id-edad")
    validarEdad()
    edadCliente = edadCliente.value
    if (edadCliente == '' || isNaN(edadCliente)) {
        return
    }
    edadCliente = parseInt(edadCliente)

    mailCliente = document.getElementById("id-email")
    validarMail()
    mailCliente = mailCliente.value
    if (mailCliente == '') {
        return
    }

    //con todos los campos cargados realizo los calculos del prestamo
    let montoCuota = calcularPrestamo(montoPrestamo, cuotasPrestamo);
    let calculoInteresTotal = (valorCuota, cantCuotas, montoSolicitado) => {
        return (valorCuota * cantCuotas) - montoSolicitado
    }
    let interesTotal = calculoInteresTotal(montoCuota, cuotasPrestamo, montoPrestamo)
    interesTotal = Math.round(interesTotal * 100) / 100;

    // solo si pude calcular correctamente guardo y muestro los resultados
    if (interesTotal > 0) {
        mostrarConfirmacionPrestamo()

        let idCliente = 0
        const resultado = comprobarIdCliente()
        // controlo que no exista un id para el dni ingresado
        if (resultado === undefined) {
            idCliente = generarIdCliente()
            clientes.push({ idCliente, nombreCliente, apellidoCliente, edadCliente, dniCliente })
            localStorage.setItem("clientes", JSON.stringify(clientes))
            actualizarLSclientes()
        } else {
            //  en caso que exista mantengo el nombre y apellido de la primera vez que se registraron los datos
            nombreCliente = resultado.nombreCliente
            apellidoCliente = resultado.apellidoCliente
            dniCliente = resultado.dniCliente
            idCliente = resultado.idCliente
        }

        // genero un id para el prestamo en curso
        let idPrestamo = buscarUltimoIdPrestamo()
        // registro el prestamo en el array de prestamos otorgados
        prestamosOtorgados.push({ idPrestamo, idCliente, monto: montoPrestamo, cuotas: cuotasPrestamo })
        localStorage.setItem("prestamosOtorgados", JSON.stringify(prestamosOtorgados))
        // envio correo electronico a la casilla que ingreso el usuario
        enviarMail(nombreCliente, montoPrestamo, cuotasPrestamo, montoCuota, interesTotal, mailCliente)
        // actualizo LS
        actualizarLSprestamosOtorgados()
        // armo tabla de detalle mostrada al pie de la pagina
        mostrarResumenPrestamo(nombreCliente, apellidoCliente, montoPrestamo, cuotasPrestamo, montoCuota, interesTotal);
        // vacio todos los inputs para registrar otro prestamo
        limpiarInputs()
    } else {
        // muestro tabla con texto de error
        errorResumenPrestamo();
    }
}

// funcion para mostrar modal de confirmacion
function mostrarConfirmacionPrestamo() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Su prestamo ha sido confirmado!',
        showConfirmButton: false,
        timer: 1500
    })
}

//armo el detalle de cuotas y una tabla en el html
function mostrarResumenPrestamo(nombre, apellido, monto, ctas, valor, interes) {
    const contenedorTabla = document.getElementById("div-resumen")
    contenedorTabla.innerHTML = ""

    const tablaPrestamo = document.createElement("table")
    tablaPrestamo.className = 'table table-bordered'

    const tituloResumen = document.createElement("strong")
    tituloResumen.innerText = "Resumen de tu prestamo"
    contenedorTabla.append(tituloResumen)

    const divisorTabla = document.createElement("hr")
    contenedorTabla.append(divisorTabla)

    const trTablaPrestamo = document.createElement("tr")
    let thNombre = document.createElement("th")
    thNombre.innerText = "Nombre"
    let thApellido = document.createElement("th")
    thApellido.innerText = "Apellido"
    let thMonto = document.createElement("th")
    thMonto.innerText = "Monto"
    let thCuotas = document.createElement("th")
    thCuotas.innerText = "Cant. cuotas"
    let thValorCta = document.createElement("th")
    thValorCta.innerText = "Valor de cuotas"
    let thInteres = document.createElement("th")
    thInteres.innerText = "Interes total"

    trTablaPrestamo.append(thNombre)
    trTablaPrestamo.append(thApellido)
    trTablaPrestamo.append(thMonto)
    trTablaPrestamo.append(thCuotas)
    trTablaPrestamo.append(thValorCta)
    trTablaPrestamo.append(thInteres)

    tablaPrestamo.append(trTablaPrestamo)

    const trTablaPrestamoFila = document.createElement("tr")

    let tdNombre = document.createElement("td")
    tdNombre.innerText = `${nombre}`
    let tdApellido = document.createElement("td")
    tdApellido.innerText = `${apellido}`
    let tdMonto = document.createElement("td")
    tdMonto.innerText = `${monto}`
    let tdCuotas = document.createElement("td")
    tdCuotas.innerText = `${ctas}`
    let tdValorCta = document.createElement("td")
    tdValorCta.innerText = `${valor}`
    let tdInteres = document.createElement("td")
    tdInteres.innerText = `${interes}`

    trTablaPrestamoFila.append(tdNombre)
    trTablaPrestamoFila.append(tdApellido)
    trTablaPrestamoFila.append(tdMonto)
    trTablaPrestamoFila.append(tdCuotas)
    trTablaPrestamoFila.append(tdValorCta)
    trTablaPrestamoFila.append(tdInteres)

    tablaPrestamo.append(trTablaPrestamoFila)

    contenedorTabla.append(tablaPrestamo)
}

//  armo tabla de resumen en opcion de consulta
function mostrarConsultaPrestamo(nombre, apellido, monto, ctas) {
    const contenedorTabla = document.getElementById("div-coincidencias")
    contenedorTabla.innerHTML = ""

    const tablaPrestamo = document.createElement("table")
    tablaPrestamo.className = 'table table-bordered'

    const tituloResumen = document.createElement("strong")
    tituloResumen.innerText = "Resumen de tu prestamo"
    contenedorTabla.append(tituloResumen)

    const divisorTabla = document.createElement("hr")
    contenedorTabla.append(divisorTabla)

    const trTablaPrestamo = document.createElement("tr")
    let thNombre = document.createElement("th")
    thNombre.innerText = "Nombre"
    let thApellido = document.createElement("th")
    thApellido.innerText = "Apellido"
    let thMonto = document.createElement("th")
    thMonto.innerText = "Monto"
    let thCuotas = document.createElement("th")
    thCuotas.innerText = "Cant. cuotas"

    trTablaPrestamo.append(thNombre)
    trTablaPrestamo.append(thApellido)
    trTablaPrestamo.append(thMonto)
    trTablaPrestamo.append(thCuotas)

    tablaPrestamo.append(trTablaPrestamo)

    const trTablaPrestamoFila = document.createElement("tr")

    let tdNombre = document.createElement("td")
    tdNombre.innerText = `${nombre}`
    let tdApellido = document.createElement("td")
    tdApellido.innerText = `${apellido}`
    let tdMonto = document.createElement("td")
    tdMonto.innerText = `${monto}`
    let tdCuotas = document.createElement("td")
    tdCuotas.innerText = `${ctas}`

    trTablaPrestamoFila.append(tdNombre)
    trTablaPrestamoFila.append(tdApellido)
    trTablaPrestamoFila.append(tdMonto)
    trTablaPrestamoFila.append(tdCuotas)

    tablaPrestamo.append(trTablaPrestamoFila)

    contenedorTabla.append(tablaPrestamo)

}

// funcion para mostrar en caso de no poder calcular el prestamo
function errorResumenPrestamo() {
    const contenedorTabla = document.getElementById("div-coincidencias")
    contenedorTabla.innerHTML = ""
    const errorResumen = document.createElement("h5")
    errorResumen.innerText = "Error - No fue posible generar el prestamo"
    contenedorTabla.append(errorResumen)
}

// funcion que devuelve las coincidencias en la busqueda por nombre o apellido
function buscarPrestamoPorNombre() {
    let nombreApellido = inputBusquedaPorNombre.value
    if (nombreApellido !== '') {
        // aplico filtro segun el dato ingresado por pantalla. primero busco por nombre y si no encuentro, busco por apellido
        const clientesFiltrados = clientesLS.filter((cliente) => {
            const nombreConApellido = cliente.nombreCliente + " " + cliente.apellidoCliente
            return nombreConApellido.includes(nombreApellido)
        })

        mostrarCoincidenciasPrestamo(clientesFiltrados)
    }
}

// funcion para armar el html con el resultado de la busqueda
function mostrarCoincidenciasPrestamo(listaClientes) {
    const contenedorDiv = document.getElementById("div-coincidencias")
    contenedorDiv.innerHTML = ""
    const ul = document.createElement("ul")
    for (const cliente of listaClientes) {
        const li = document.createElement("li")

        for (const prestamos of prestamosOtorgadosLS) {
            if (cliente.idCliente == prestamos.idCliente) {
                li.innerText = `Prestamo ID: ${prestamos.idPrestamo} -- ${cliente.nombreCliente} ${cliente.apellidoCliente}`
            }
        }

        ul.append(li)
    }
    contenedorDiv.append(ul)
}

// busco el prestamo dentro mi LS de prestamos otorgados
function buscarPrestamoPorId() {
    // recorro ambos arrays, primero en prestamos otorgados con el id ingresado y si lo encuentro, voy al siguiente array de clientes para buscar los datos de la persona  
    let idBusqueda = document.getElementById("id-key").value

    idBusqueda = parseInt(idBusqueda)
    if (isNaN(idBusqueda)) {
        document.getElementById("id-busqid").disabled = true
        return
    }

    let encontrado = false
    for (let prestamos of prestamosOtorgadosLS) {
        if (prestamos.idPrestamo === idBusqueda) {
            let idCliente = prestamos.idCliente
            for (let cliente of clientesLS) {
                if (idCliente === cliente.idCliente) {
                    encontrado = true
                    mostrarConsultaPrestamo(cliente.nombreCliente, cliente.apellidoCliente, prestamos.monto, prestamos.cuotas);
                }
            }
        }
    }

    if (!encontrado) {
        Swal.fire({
            title: 'No existe el ID: ' + idBusqueda,
            icon: 'info',
            confirmButtonText: 'OK'
        })
    }

    document.getElementById("id-key").value = ''
}

// funcion para solicitar el monto del prestamo y controlar los topes
function validarMonto() {
    if (montoPrestamo.value >= 500 && montoPrestamo.value <= 500000) {
        return
    } else {
        switch (true) {
            case montoPrestamo.value == '':
                Swal.fire({
                    title: 'Debe completar el monto',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break
            case montoPrestamo.value <= 500:
                Swal.fire({
                    title: 'El monto minimo es $500',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break;
            case montoPrestamo.value > 500000:
                Swal.fire({
                    title: 'El monto maximo no puede ser mayor a $500.000',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break
            default:
                break
        }
        montoPrestamo.value = ''
        document.getElementById("id-monto").focus()
    }
}

// funcion para solicitar el nombre del cliente
function validarNombre() {
    if (nombreCliente.value == '') {
        Swal.fire({
            title: 'Debe completar su nombre.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })
        nombreCliente.focus()
        return
    }

    if (nombreCliente.value >= 0) {
        Swal.fire({
            title: 'Su nombre no puede contener numeros.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })
        nombreCliente.value = ''
        nombreCliente.focus()
        return
    }
}

// funcion para solicitar el apellido del cliente
function validarApellido() {
    if (apellidoCliente.value == '') {
        Swal.fire({
            title: 'Debe completar su apellido.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })

        apellidoCliente.focus()
        return
    }

    if (apellidoCliente.value >= 0) {
        Swal.fire({
            title: 'Su apellido no puede contener numeros.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })
        apellidoCliente.value = ''
        apellidoCliente.focus()
        return
    }
}

// funcion para solicitar dni del cliente
function validarDNI() {
    if (dniCliente.value == 0) {
        Swal.fire({
            title: 'Debe completar su numero de documento.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })
        document.getElementById("id-dni").value = ''
        dniCliente.focus()
        return
    }
}

// funcion para solicitar la edad del cliente y controlar topes
function validarEdad() {
    let auxEdad = document.getElementById("id-edad").value
    auxEdad = parseInt(auxEdad)
    if (auxEdad >= 18 && auxEdad <= 70) {
        return
    } else {
        switch (true) {
            case auxEdad < 18:
                Swal.fire({
                    title: `Edad ${auxEdad} invalida. Debe tener al menos 18 años.`,
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break;
            case auxEdad > 70:
                Swal.fire({
                    title: `Edad ${auxEdad} invalida. Debe ser menor a 70 años.`,
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break
            case auxEdad == '':
            default:
                Swal.fire({
                    title: `Debe completar su edad`,
                    icon: 'warning',
                    confirmButtonText: 'OK'
                })
                break
        }
        document.getElementById("id-edad").value = ''
    }
}

// funcion para solicitar email del cliente (debe ser uno existente para que efectivamente llegue el mail)
function validarMail() {
    if (mailCliente.value == 0) {
        Swal.fire({
            title: 'Debe completar su email.',
            icon: 'warning',
            confirmButtonText: 'OK'
        })
        document.getElementById("id-email").value = ''
        mailCliente.focus()
        return
    }
}

// armo una tasa de interes ficticia, evaluando segun la cuota elegida
function buscarTasaInteres(cuotas) {
    let tasaInteres = 0
    switch (true) {
        case cuotas >= 1 && cuotas <= 3:
            tasaInteres = 25
            break
        case cuotas >= 4 && cuotas <= 6:
            tasaInteres = 40
            break
        case cuotas >= 7 && cuotas <= 9:
            tasaInteres = 60
        default:
            tasaInteres = 80
            break
    }
    // creo una tasa de interes mensual para aplicar cant de cuotas
    tasaInteres = parseFloat(tasaInteres / 12)
    return tasaInteres.toFixed(2)
}

// funcion para calcular el prestamo aplicando la tasa de interes segun el plan seleccionado
function calcularPrestamo(monto, cuotas) {
    let tasaPrestamo = buscarTasaInteres(cuotas)
    let montoCuota = (monto / cuotas) * (1 + ((tasaPrestamo * cuotas) / 100))
    return montoCuota.toFixed(2)
}

// funcion para verificar si existe el cliente
function comprobarIdCliente() {
    // busco en mi array de clientes si existe el dni, y en ese caso retorno el id de la persona
    resultado = clientes.find(cliente => {
        return cliente.dniCliente === dniCliente
    })
    if (resultado != undefined) {
        return resultado
    }
}

// funcion para generar un nuevo id de cliente
function generarIdCliente() {
    // busco el ult id y le sumo 1
    const ultimo = clientes.length - 1
    const ultimoIdCliente = clientes[ultimo].idCliente
    return ultimoIdCliente + 1
}

// funcion para buscar el ultimo prestamo otorgado
function buscarUltimoIdPrestamo() {
    const ultimo = prestamosOtorgados.length - 1
    const ultimoIdPrestamo = prestamosOtorgados[ultimo].idPrestamo
    return ultimoIdPrestamo + 1
}

// habilita la vista de mene "solicitar prestamo" y oculta el resto
function mostrarSolicitar() {
    document.getElementById("div-solicitarPrestamos").style.display = 'block'
    document.getElementById("div-consultarPrestamos").style.display = 'none'
    document.getElementById("div-menu-seleccion").style.display = 'none'
}

// habilita la vista de mene "consultar prestamo" y oculta el resto
function mostrarConsultar() {
    document.getElementById("div-solicitarPrestamos").style.display = 'none'
    document.getElementById("div-consultarPrestamos").style.display = 'block'
    document.getElementById("div-menu-seleccion").style.display = 'none'
}

// habilita el boton para buscar por id en el menu de consultas
function habilitarBtn() {
    document.getElementById("id-busqid").disabled = false
}

// vacia el contenido del nombre o apellido en el menu de consultas
function limpiarInputNombre() {
    document.getElementById("id-nomape").value = ''
    const contenedorDiv = document.getElementById("div-coincidencias")
    contenedorDiv.innerHTML = ""
}

// funcion para vacias todos los campos
function limpiarInputs() {
    document.getElementById("id-monto").value = ''
    document.getElementById("id-nombre").value = ''
    document.getElementById("id-apellido").value = ''
    document.getElementById("id-dni").value = ''
    document.getElementById("id-edad").value = ''
    document.getElementById("id-email").value = ''
}

// funcion utilizada para actualiar el LS de clientes (viene del array clientes)
function actualizarLSclientes() {
    clientesLocalStorage = localStorage.getItem("clientes")
    // valido si ya existen LS anteriores
    if (clientesLocalStorage == null) {
        // almaceno datos predeterminados en localstorage     
        localStorage.setItem("clientes", JSON.stringify(clientes))
        // los recupero de LS
        clientesLocalStorage = localStorage.getItem("clientes")
        // lo guardo en array especifico para clientes de LS
        clientesLS = JSON.parse(clientesLocalStorage)
    } else {
        // lo guardo en array especifico para clientes de LS
        clientesLS = JSON.parse(clientesLocalStorage)
    }
}

// funcion utilizada para actualiar el LS de prestamos (viene del array prestamos otorgados)
function actualizarLSprestamosOtorgados() {
    prestamosLocalStorage = localStorage.getItem("prestamosOtorgados")
    if (prestamosLocalStorage == null) {
        localStorage.setItem("prestamosOtorgados", JSON.stringify(prestamosOtorgados))
        prestamosLocalStorage = localStorage.getItem("prestamosOtorgados")
        prestamosOtorgadosLS = JSON.parse(prestamosLocalStorage)
    } else {
        prestamosOtorgadosLS = JSON.parse(prestamosLocalStorage)
    }
}

// funcion para buscar el archivo json una base de clientes predefinida
// obligo que la promesa se resuelva para tener actualizado el LS
async function obtenerBaseClientes() {
    const respuesta = await fetch('/baseclientes.json')
    const baseClientes = await respuesta.json()
    for (const cliente of baseClientes) {
        clientes.push(cliente)
    }
    //cargo local storage de clientes
    actualizarLSclientes()
}

// funcion para buscar el archivo json una base de prestamos predefinida
// obligo que la promesa se resuelva para tener actualizado el LS
async function obtenerBasePrestamos() {
    const respuesta = await fetch('/baseprestamos.json')
    const basePrestamos = await respuesta.json()
    for (const prestamo of basePrestamos) {
        prestamosOtorgados.push(prestamo)
    }
    //cargo local storage de prestamos
    actualizarLSprestamosOtorgados()
}

// funcion que utiliza API EmailJS para enviar mail de confirmacion
function enviarMail(nombre, montoPrestamo, cantCuotas, montoCuota, interesTotal, mail) {
    const serviceID = 'default_service';
    const templateID = 'template_ntenifh';
    const publicKey = 'JBVa7vpIKq9pUFArw';
    fetch(urlBaseMail, {
        method: 'POST',
        body: JSON.stringify({
            service_id: serviceID,
            template_id: templateID,
            user_id: publicKey,
            template_params: {
                'nombreCliente': nombre,
                'montoPrestamo': montoPrestamo,
                'cantCuotas': cantCuotas,
                'montoCuota': montoCuota,
                'interesTotal': interesTotal,
                'destino': mail
            }
        }),
        headers: {
            'Content-type': 'application/json'
        },
    }).then((res) => {
        if (res.status === 200) { notificacionTosty() }
    })
}

// funcion para notificar en pantalla si el envio de mail ha sido exitoso
function notificacionTosty() {
    Toastify({
        text: "Mail de confirmación enviado!",
        duration: 2500,
        gravity: 'bottom',
        className: "info",
        style: {
            background: "linear-gradient(to right, #3355FF, #868DB0)",
        }
    }).showToast();
}
